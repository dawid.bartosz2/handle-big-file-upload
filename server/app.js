const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');
const fs = require('fs');
const md5 = require('md5');

const indexRouter = require('./routes/index');
const app = express();

// app.use(express.urlencoded({ extended: false }));

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.raw({ type:'application/octet-stream', limit:'80mb' }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use('/uploads', express.static('uploads'));

app.post('/upload', (req, res) => {
    const name = req.get('Uploading-File-Name');
    const totalChunks = req.get('Uploading-File-Total-Chunks');
    const currentChunkIndex = req.get('Uploading-File-Chunk-Index');

    if (!name || !name.includes('.')) {
        return res.status(400).json({ status: 'error', errorMessage: `Incorrect 'Uploading-File-Name' header.`})
    }

    if (!totalChunks || parseInt(totalChunks) < 1) {
        return res.status(400).json({ status: 'error', errorMessage: `Incorrect 'Uploading-File-Total-Chunks' header.`})
    }

    if (!currentChunkIndex || parseInt(currentChunkIndex) < 0 || parseInt(currentChunkIndex) > parseInt(totalChunks)) {
        return res.status(400).json({ status: 'error', errorMessage: `Incorrect 'Uploading-File-Chunk-Index' header.`})
    }

    const isFirstChunk = parseInt(currentChunkIndex) === 0;
    const isLastChunk = parseInt(currentChunkIndex) === parseInt(totalChunks) -1;

    const [fileName, fileExtension] = name.split('.');
    const chunk = req.body.toString().split(',')[1];

    const buffer = new Buffer.from(chunk, 'base64');
    const uniqueFileName = md5(name + req.ip)
    const tmpFileName = `tmp_${uniqueFileName}.${fileExtension}`;

    if (isFirstChunk && fs.existsSync(`./uploads/${tmpFileName}`)) {
        fs.unlinkSync(`./uploads/${tmpFileName}`);
    }

    fs.appendFileSync(`./uploads/${tmpFileName}`, buffer);

    if (isLastChunk) {
        const timestampHash = md5(Date.now()).substr(0, 6);
        const finalFileName = `${timestampHash}_${fileName}.${fileExtension}`;

        fs.renameSync(`./uploads/${tmpFileName}`, `./uploads/${finalFileName}`);

        return res.json({ status: 'success', finalFileName, originalFileName: name });
    }

    res.json({ status: 'in progress', chunksLeft: Math.ceil(parseInt(totalChunks) - parseInt(currentChunkIndex) - 1) });
});

app.use((error, req, res, next) => {
    console.log("Error Handling Middleware called");
    console.log('Path: ', req.path);
    console.error('Error: ', error);

    if (error.type === 'time-out') {
        return res.status(408).send(error);
    }

    res.status(500).send(error);
})

module.exports = app;
