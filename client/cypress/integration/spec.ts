import 'cypress-file-upload';

describe('Home Page', () => {
  const fixtureFile = 'photo.png';
  const fixtureFiles = ['photo.png', 'recording.mov'];

  beforeEach(() => {
    cy.visit('/');
  });

  it('Should have these texts', () => {
    cy.contains('File upload');
    cy.contains('Files');
    cy.contains('Upload file');
  });

  it('Should successfully upload single (small size) file', () => {
    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFile);
    cy.get('button[data-cy="file-upload-button"]').click();

    const timeout = 1000;

    cy.get('li[data-cy="files-upload-feedback"]', { timeout }).should(
      'have.length',
      1
    );
    cy.get('div[class="file-upload__result"]', { timeout }).should(
      'have.css',
      'background-color',
      'rgb(0, 128, 0)'
    );

    cy.get('span[data-cy="feedback-status"]', { timeout }).contains('success', {
      matchCase: false
    });
    cy.get('span[data-cy="feedback-original-file-name"]', { timeout }).contains(
      'photo.png'
    );
    cy.get('span[data-cy="feedback-final-file-name"]', { timeout }).should(
      'exist'
    );
  });

  it('Should successfully upload multiple (one large and one small size) files', () => {
    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFiles);
    cy.get('button[data-cy="file-upload-button"]').click();

    cy.wait(3000);

    cy.get('li[data-cy="files-upload-feedback"]').should('have.length', 2);
    cy.get('div[class="file-upload__result"]').should(
      'have.css',
      'background-color',
      'rgb(0, 128, 0)'
    );

    cy.get('span[data-cy="feedback-status"]').should('have.length', 2);
    cy.get('span[data-cy="feedback-final-file-name"]').should('have.length', 2);

    cy.get('span[data-cy="feedback-original-file-name"]').should(
      'have.length',
      2
    );
    cy.contains(
      'span[data-cy="feedback-original-file-name"]',
      fixtureFiles[0]
    ).should('exist');
    cy.contains(
      'span[data-cy="feedback-original-file-name"]',
      fixtureFiles[1]
    ).should('exist');
  });

  it('Should show an error regarding incorrect file name header', () => {
    cy.intercept('http://localhost:3000/upload', (req) => {
      req.headers['Uploading-File-Name'] = 'file_name_without_extension'; // no header or no '.' for splitting name from extension
    }).as('upload');

    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFile);
    cy.get('button[data-cy="file-upload-button"]').click();

    cy.wait('@upload')
      .its('request.headers')
      .should(
        'have.a.property',
        'Uploading-File-Name',
        'file_name_without_extension'
      );

    const timeout = 1000;

    cy.get('li[data-cy="files-upload-feedback"]', { timeout }).should(
      'have.length',
      1
    );
    cy.get('div[class="file-upload__result"]', { timeout }).should(
      'have.css',
      'background-color',
      'rgb(128, 0, 0)'
    );

    cy.get('span[data-cy="feedback-status"]', { timeout }).contains('error', {
      matchCase: false
    });
    cy.get('span[data-cy="feedback-error-message"]', { timeout }).contains(
      `Incorrect 'Uploading-File-Name' header.`
    );
  });

  it('Should show an error regarding incorrect total chunks header', () => {
    cy.intercept('http://localhost:3000/upload', (req) => {
      req.headers['Uploading-File-Total-Chunks'] = '0'; // no header or any value < 1
    }).as('upload');

    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFile);
    cy.get('button[data-cy="file-upload-button"]').click();

    cy.wait('@upload')
      .its('request.headers')
      .should('have.a.property', 'Uploading-File-Total-Chunks', '0');

    const timeout = 1000;

    cy.get('li[data-cy="files-upload-feedback"]', { timeout }).should(
      'have.length',
      1
    );
    cy.get('div[class="file-upload__result"]', { timeout }).should(
      'have.css',
      'background-color',
      'rgb(128, 0, 0)'
    );

    cy.get('span[data-cy="feedback-status"]', { timeout }).contains('error', {
      matchCase: false
    });
    cy.get('span[data-cy="feedback-error-message"]', { timeout }).contains(
      `Incorrect 'Uploading-File-Total-Chunks' header.`
    );
  });

  it('Should show an error regarding too small file chunk index header', () => {
    cy.intercept('http://localhost:3000/upload', (req) => {
      req.headers['Uploading-File-Chunk-Index'] = '-1'; // no header or any value < 0 or any value bigger than 'Uploading-File-Total-Chunks' header
    }).as('upload');

    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFile);
    cy.get('button[data-cy="file-upload-button"]').click();

    cy.wait('@upload')
      .its('request.headers')
      .should('have.a.property', 'Uploading-File-Chunk-Index', '-1');

    const timeout = 1000;

    cy.get('li[data-cy="files-upload-feedback"]', { timeout }).should(
      'have.length',
      1
    );
    cy.get('div[class="file-upload__result"]', { timeout }).should(
      'have.css',
      'background-color',
      'rgb(128, 0, 0)'
    );

    cy.get('span[data-cy="feedback-status"]', { timeout }).contains('error', {
      matchCase: false
    });
    cy.get('span[data-cy="feedback-error-message"]', { timeout }).contains(
      `Incorrect 'Uploading-File-Chunk-Index' header.`
    );
  });

  it('Should show an error regarding too big file chunk index header', () => {
    cy.intercept('http://localhost:3000/upload', (req) => {
      req.headers['Uploading-File-Chunk-Index'] = '2'; // no header or any value < 0 or any value bigger than 'Uploading-File-Total-Chunks' header
      req.headers['Uploading-File-Total-Chunks'] = '1';
    }).as('upload');

    cy.get('input[data-cy="file-upload-input"]').attachFile(fixtureFile);
    cy.get('button[data-cy="file-upload-button"]').click();

    cy.wait('@upload')
      .its('request.headers')
      .should('have.a.property', 'Uploading-File-Chunk-Index', '2');
    cy.wait('@upload')
      .its('request.headers')
      .should('have.a.property', 'Uploading-File-Total-Chunks', '1');

    const timeout = 1000;

    cy.get('li[data-cy="files-upload-feedback"]', { timeout }).should(
      'have.length',
      1
    );
    cy.get('div[class="file-upload__result"]', { timeout }).should(
      'have.css',
      'background-color',
      'rgb(128, 0, 0)'
    );

    cy.get('span[data-cy="feedback-status"]', { timeout }).contains('error', {
      matchCase: false
    });
    cy.get('span[data-cy="feedback-error-message"]', { timeout }).contains(
      `Incorrect 'Uploading-File-Chunk-Index' header.`
    );
  });
});
