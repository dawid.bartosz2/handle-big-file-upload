import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { forkJoin, from, Observable, of } from 'rxjs';
import { catchError, concatMap, last, map, retry } from 'rxjs/operators';

export interface ChunkUploadHeaders {
  'Content-Type': string;
  'Uploading-File-Name': string;
  'Uploading-File-Size': string;
  'Uploading-File-Total-Chunks': string;
  'Uploading-File-Chunk-Index': string;
}

export type FileUploadStatus = 'success' | 'in progress' | 'error';

export interface FileUploadResponse {
  status: FileUploadStatus;
  originalFileName?: string;
  finalFileName?: string;
  errorMessage?: string;
  chunksLeft?: number;
}

export const ChunkUploadHeadersFactory = (
  fileName: string,
  fileSize: number,
  chunksAmount: number,
  chunkIndex: number
): ChunkUploadHeaders => ({
  'Content-Type': 'application/octet-stream',
  'Uploading-File-Name': fileName,
  'Uploading-File-Size': fileSize.toString(),
  'Uploading-File-Total-Chunks': chunksAmount.toString(),
  'Uploading-File-Chunk-Index': chunkIndex.toString()
});

export const CHUNK_SIZE: number = 8000000;

@Injectable()
export class UploadService {
  private readonly baseURL: string = 'http://localhost:3000/upload';

  constructor(private http: HttpClient) {}

  batchFilesUpload(files: FileList): Observable<FileUploadResponse[]> {
    const actions$: Observable<FileUploadResponse>[] = Array.from(files).map(
      (file: File) => this.uploadFile(file)
    );
    return forkJoin(actions$);
  }

  uploadFile(file: File): Observable<FileUploadResponse> {
    const fileSize: number = file.size;
    const chunksAmount: number = Math.ceil(fileSize / CHUNK_SIZE);

    const chunks$: Observable<number> = from([...Array(chunksAmount).keys()]);

    return chunks$.pipe(
      concatMap((chunkIndex: number) => {
        const readFileFrom: number = chunkIndex * CHUNK_SIZE;
        const readFileTo: number = readFileFrom + CHUNK_SIZE;

        const headers: ChunkUploadHeaders = ChunkUploadHeadersFactory(
          file.name,
          fileSize,
          chunksAmount,
          chunkIndex
        );
        const blob: Blob = file.slice(readFileFrom, readFileTo);

        return this.readBlob(blob).pipe(
          map((blobData: string | ArrayBuffer) => [blobData, headers])
        );
      }),
      concatMap(
        ([blobData, headers]: [string | ArrayBuffer, ChunkUploadHeaders]) =>
          this.uploadBlob(blobData, headers)
      ),
      catchError((httpError: HttpErrorResponse) =>
        of(httpError.error as FileUploadResponse)
      ),
      last()
    );
  }

  private uploadBlob(
    blobData: string | ArrayBuffer,
    headers: ChunkUploadHeaders
  ): Observable<FileUploadResponse> {
    return this.http
      .post<FileUploadResponse>(this.baseURL, blobData, {
        headers: { ...headers }
      })
      .pipe(retry(3));
  }

  private readBlob(
    blob: Blob,
    reader: FileReader = new FileReader()
  ): Observable<string | ArrayBuffer> {
    return new Observable((obs) => {
      if (!(blob instanceof Blob)) {
        obs.error(new Error('`blob` must be an instance of File or Blob.'));
        return;
      }

      reader.onerror = (err) => obs.error(err);
      reader.onabort = (err) => obs.error(err);
      reader.onload = () => obs.next(reader.result);
      reader.onloadend = () => obs.complete();

      return reader.readAsDataURL(blob);
    });
  }
}
