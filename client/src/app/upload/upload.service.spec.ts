import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { CHUNK_SIZE, UploadService } from './upload.service';

function MockFile({
  name = 'file.txt',
  size = 1024,
  type = 'plain/txt',
  lastModified = new Date()
}): File {
  const blob = new Blob(['a'.repeat(size)], { type });

  blob['lastModifiedDate'] = lastModified;

  return new File([blob], name);
}

// describe('Upload Service', () => {
//   let service: UploadService;
//   let httpClientSpy: jasmine.SpyObj<HttpClient>;
//
//   beforeEach(() => {
//     httpClientSpy = jasmine.createSpyObj('HttpClient', ['post', 'get']);
//     service = new UploadService(httpClientSpy);
//   });
//
//   it('should return expected heroes (HttpClient called once)', (done: DoneFn) => {
//     httpClientSpy.get.and.returnValue(of('XD'));
//
//     service.test().subscribe(
//       (response: any) => {
//         expect(response).toEqual('XD', 'success response');
//         done();
//       },
//     );
//
//     expect(httpClientSpy.get.calls.count()).toBe(1, '1 call');
//   });
//
//   it('should return expected response after file upload', (done: DoneFn) => {
//     const mockFile: File = MockFile({
//       name: 'file.txt',
//       size: 1000000,
//       // size: 1024 * 1024 * 3,
//       type: 'plain/txt',
//     });
//
//     const fileSize: number = mockFile.size;
//     const chunksAmount: number = Math.ceil(fileSize / CHUNK_SIZE);
//
//     const expectedUrl = 'http://localhost:3000/upload';
//
//     const mockResponse = (chunkIndex) => ({ status: 'in progress', chunksLeft: Math.ceil(chunksAmount - chunkIndex - 1) });
//     const successResponse = { status: 'success', fileName: mockFile.name };
//
//     spyOn(httpClientSpy)
//     httpClientSpy.post.and.returnValue(of(successResponse));
//
//     service.uploadFile(mockFile).subscribe(
//       (response: any) => {
//         expect(response).toEqual(successResponse, 'success response');
//         done();
//       },
//     );
//
//     expect(httpClientSpy.post.calls.count()).toBe(1, '1 call');
//   });
// });

describe('UploadService', () => {
  let service: UploadService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UploadService]
    });

    service = TestBed.inject(UploadService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be initialized', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected response', (done: DoneFn) => {
    const mockFile: File = MockFile({
      name: 'file.txt',
      size: 1024 * 1024 * 3,
      type: 'plain/txt'
    });

    const fileSize: number = mockFile.size;
    const chunksAmount: number = Math.ceil(fileSize / CHUNK_SIZE);

    const expectedUrl = 'http://localhost:3000/upload';

    const mockResponse = (chunkIndex) => ({
      status: 'in progress',
      chunksLeft: Math.ceil(chunksAmount - chunkIndex - 1)
    });
    const successResponse = { status: 'success', finalFileName: mockFile.name };

    service.uploadFile(mockFile).subscribe((response: any) => {
      expect(response?.status).toEqual(
        'success',
        'response should be equal to the success response'
      );
      done();
    });

    const requests = httpTestingController.match(expectedUrl);
    console.log({ requests });

    expect(requests.length).toBe(chunksAmount);

    Array(chunksAmount).forEach((_, index) => {
      const response =
        index < chunksAmount - 1 ? mockResponse(index) : successResponse;
      requests[index].flush(response);
    });
  });
});
