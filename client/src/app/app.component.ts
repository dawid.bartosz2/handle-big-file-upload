import { Component, OnDestroy } from '@angular/core';
import { FileUploadResponse, UploadService } from './upload/upload.service';
import { Observable, Subscription, tap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  files: FileList | undefined;
  filesUploadFeedback: FileUploadResponse[] | undefined;

  canUploadFiles: boolean = false;
  isUploadingFiles: boolean = false;
  isFilesUploadCompleted: boolean = false;

  subscriptions: Subscription = new Subscription();

  constructor(private uploadService: UploadService) {}

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  getFile(event: Event): void {
    const { files } = event.target as HTMLInputElement;

    if (!files) {
      return;
    }

    this.canUploadFiles = true;
    this.files = files;
  }

  uploadFile(): void {
    if (!this.files?.length) {
      return;
    }

    const action$: any =
      this.files.length > 1
        ? this.uploadService.batchFilesUpload(this.files)
        : this.uploadService.uploadFile(this.files[0]);

    const sub = action$
      .pipe(tap(() => (this.isUploadingFiles = true)))
      .subscribe({
        next: (feedback) => (this.filesUploadFeedback = [feedback].flat()),
        error: (feedback) => (this.filesUploadFeedback = [feedback].flat()),
        complete: () => {
          this.isFilesUploadCompleted = true;
          this.isUploadingFiles = false;
        }
      });

    this.subscriptions.add(sub);
  }
}
