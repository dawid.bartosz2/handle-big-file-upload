# Handle Big File Upload

## [Short presentation](https://www.screencast.com/t/DL3FghwQBf7)

## How it works
You can upload multiple files in chunks. Files output is **/handle-big-file-upload/server/uploads**.

## Installation
Handle Big File Upload requires [Node.js](https://nodejs.org/) v12+ to run.
Enter the **handle-big-file-upload** directory and install the dependencies.

```sh
cd handle-big-file-upload
sh setup.sh


// if the above shell script didn't work
cd server/
npm install

cd ../client/
npm install
```

## How to run locally
Enter **server** subdirectory and start server in the first terminal.
```sh
cd server/
npm start
```

Enter **client** subdirectory and start the client app in the second terminal. App should be automatically opened in your browser.
```sh
cd client/
npm start
```

## How to run e2e tests in cypress
```sh
cd client
npm run e2e
```
